<?php

Route::pattern('id', '\d+');

Route::group(array('prefix' => 'tables', 'namespace' => 'Table'), function()
{

	Route::get('/',
	[ 
		'as'   => 'dash.tables.getIndex',
		'uses' => 'DashTableController@getIndex'
	]);

	/*Route::get('{id}',
	[ 
		'as'   => 'dash.tables.getShow',
		'uses' => 'DashTableController@getShow'
	]);*/

	Route::get('create',
	[ 
		'as'   => 'dash.tables.getCreate',
		'uses' => 'DashTableController@getCreate'
	]);

	Route::post('/',
	[ 
		'as'   => 'dash.tables.postCreate',
		'uses' => 'DashTableController@postCreate'
	]);

	Route::get('{id}/edit',
	[ 
		'as'   => 'dash.tables.getUpdate',
		'uses' => 'DashTableController@getUpdate'
	]);
	
	Route::put('{id}',
	[ 
		'as'   => 'dash.tables.putUpdate',
		'uses' => 'DashTableController@putUpdate'
	]);

	Route::patch('{id}',
	[ 
		'as'   => 'dash.tables.patchUpdate',
		'uses' => 'DashTableController@patchUpdate'
	]);

	Route::get('{id}',
	[ 
		'as'   => 'dash.tables.deleteDelete',
		'uses' => 'DashTableController@deleteDelete'
	]);
	
	Route::get('chart',
	[ 
		'as'   => 'dash.tables.getChart',
		'uses' => 'DashTableController@getChart'
	]);
	
	Route::get('resetAll',
	[ 
		'as'   => 'dash.tables.resetAll',
		'uses' => 'DashTableController@resetAll'
	]);

});
