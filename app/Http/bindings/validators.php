<?php

/*
|--------------------------------------------------------------------------
| Validators
|--------------------------------------------------------------------------
*/

App::bind('TableValidator', function()
{
    return new \App\Http\Validators\TableValidator;
});