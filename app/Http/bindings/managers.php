<?php

/*
|--------------------------------------------------------------------------
| Managers
|--------------------------------------------------------------------------
*/

App::bind('TableManager', function($app,$param)
{
    return new \App\Http\Managers\TableManager($param['delegate']);
});
