<?php namespace App\Http\Presenters;

use Laracasts\Presenter\Presenter;

class AdPresenter extends BasePresenter
{

    public function __construct($resource)
    {
        $this->resource = $resource;
    }

}