<?php

Form::macro("activeSwitch",function($name)
{
	return Form::checkbox($name,1,null,['class'=>'switch', 'data-on-text'=>'Yes', 'data-off-text'=>'No', 'data-on-color'=>'success', 'data-off-color'=>'danger', 'data-size'=>'normal']);
});


/*
|--------------------------------------------------------------------------
|
|--------------------------------------------------------------------------
*/


Form::macro("assets",function($section)
{
	$markup = "";

	$assets = Config::get("asset");

	if (isset($assets[$section]))
	{
		foreach ($assets[$section] as $key => $value)
		{
			$use = $value;

			if (is_string($key)) 
			{
				$use = $key; 
			}

			if (ends_with($use, ".css")) 
			{
		        $markup .= "<link rel='stylesheet' type='text/css' href='" . asset($use) . "'/>"; 
		    }

			if (ends_with($use, ".js")) 
			{
				$markup .= "<script type='text/javascript' src='" . asset($use) . "'></script>";
			}
		} 
	}
	
	return $markup; 
});


/*
|--------------------------------------------------------------------------
|
|--------------------------------------------------------------------------
*/


Form::macro('deleteButton',function($route, $id, $title)
{
    $format = '<a href="%s" data-toggle="tooltip" data-delete="%s" title="%s" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></i></a>';
    
    $link = URL::route($route, ['id' => $id]);
    
    $token = csrf_token();
     
    return sprintf($format, $link, $token, $title);	
});
