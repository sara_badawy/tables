<?php

/*
|--------------------------------------------------------------------------
|
|--------------------------------------------------------------------------
*/

Response::macro('ApiCreated', function()
{
	$data = 
	[
		'http_code'  => 201,
		'success'    => true,
		'msg'        => 'Created'
	];

	return Response::json($data, 201);
});

/*
|--------------------------------------------------------------------------
|
|--------------------------------------------------------------------------
*/

Response::macro('ApiUpdated', function()
{
	$data = 
	[
		'http_code'  => 200,
		'success'    => true,
		'msg'        => 'Updated'
	];

	return Response::json($data, 200);
});

/*
|--------------------------------------------------------------------------
|
|--------------------------------------------------------------------------
*/

Response::macro('notFoundException', function($data,$view)
{
	if( requestLocation() == 'api' )
	{
		return Response::json($data, 404);
	}
	elseif( requestLocation() == 'dash' )
	{
		return Response::view($view);
	}
});

/*
|--------------------------------------------------------------------------
|
|--------------------------------------------------------------------------
*/
	
Response::macro('validationException', function($errors)
{
	if( requestLocation() == 'api' )
	{
		dd('validation exception');
	}
	elseif( requestLocation() == 'dash' )
	{
		return Redirect::back()->withInput()->withError($errors);
	}
});


/*
|--------------------------------------------------------------------------
|
|--------------------------------------------------------------------------
*/