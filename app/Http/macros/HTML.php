<?php


/*
|--------------------------------------------------------------------------
|
|--------------------------------------------------------------------------
*/


HTML::macro('active', function($name,$n,$return = 'active')
{
	if($n == 1)
	{
	    return Request::segment(1) == $name && !Request::segment(2) ? $return : '';
	}
	elseif($n == 2)
	{
		if(is_array($name))
		{
			foreach($name as $segment)
			{
				if( Request::segment(2) == $segment)
				{
					return $return;
				}
			}
		}
		else
		{
			return Request::segment(2) == $name ? $return : '';	
		}
	}
	else
	{
		return Request::segment($n) == $name ? $return : '';
	}
});


/*
|--------------------------------------------------------------------------
|
|--------------------------------------------------------------------------
*/

