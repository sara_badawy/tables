<?php namespace App\Http\Validators;

class TableValidator extends BaseValidator
{
    public function __construct() 
    {
        $this->rules = 
        [
            //'category_id' => ['required','exists:categories,id'],

            'name_en'     => ['required'],

            'name_ar'     => ['required'],

            //'desc_en'     => ['required'],

            //'desc_ar'     => ['required'],

            //'price'       => ['required','numeric'],

            'in_menu'     => ['in:0,1']
		];
    }

    public function store()
    {
		$this->rules['name_en'][] = 'unique:tables,name_en';

		$this->rules['name_ar'][] = 'unique:tables,name_ar';

        return $this;
    }

    public function update()
    {
		$this->rules['name_en'][] = 'unique:tables,name_en,'.$this->ignoreId;

		$this->rules['name_ar'][] = 'unique:tables,name_ar,'.$this->ignoreId;

        return $this;
    }
}