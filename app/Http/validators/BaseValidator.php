<?php namespace App\Http\Validators;

use Validator;

abstract class BaseValidator
{
	protected $rules;

    protected $ignoreId;

	public function __construct()
	{

	}

    /*
    |--------------------------------------------------------------------------
    | 
    |--------------------------------------------------------------------------
    */

    public function ignore($id)
    {
        $this->ignoreId = $id;

        return $this;
    }

    /*
    |--------------------------------------------------------------------------
    | 
    |--------------------------------------------------------------------------
    */

    public function validate($data)
    {
        $validator = Validator::make($data, $this->rules);

        if(!$validator->passes())
        {
	        throw new \App\Exceptions\ValidationException($validator->messages());
        }
    }
}