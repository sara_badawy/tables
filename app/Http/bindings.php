<?php

require app_path().'/Http/bindings/managers.php';

require app_path().'/Http/bindings/repos.php';

require app_path().'/Http/bindings/models.php';

require app_path().'/Http/bindings/validators.php';
