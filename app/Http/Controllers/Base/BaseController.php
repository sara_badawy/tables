<?php namespace App\Http\Controllers\Base;

use App\Http\Controllers\Controller;

abstract class BaseController extends Controller 
{

	public function __construct()
	{
	
	}

	/*
	|--------------------------------------------------------------------------
	| 
	|--------------------------------------------------------------------------
	*/
	
	protected function AddCSRFFilter()
	{
		$this->beforeFilter('csrf', [ 'on' => ['post', 'put', 'delete', 'patch'] ]);
	}
}