<?php namespace App\Http\Controllers\Base;

abstract class DashBaseController extends BaseController 
{

	public function __construct()
	{
		parent::__construct();

		//$this->AddCSRFFilter();

		//$this->beforeFilter('auth', ['except' => 'getLogin,postLogin']);
	}

	/*
	|--------------------------------------------------------------------------
	| 
	|--------------------------------------------------------------------------
	*/
	
	public function storeSuccess($route)
	{
		return \Redirect::route('dash.'.$route)->withSuccess('Record has been created');
	}

	/*
	|--------------------------------------------------------------------------
	| 
	|--------------------------------------------------------------------------
	*/
	
	public function updateSuccess($route)
	{
		return \Redirect::route('dash.'.$route)->withSuccess('Record has been updated');
	}

	/*
	|--------------------------------------------------------------------------
	| 
	|--------------------------------------------------------------------------
	*/
	
	public function deleteSuccess($route)
	{
		return \Redirect::route('dash.'.$route)->withSuccess('Record has been deleted');
	}

}