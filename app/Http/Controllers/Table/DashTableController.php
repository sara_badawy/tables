<?php namespace App\Http\Controllers\Table;

use App\Http\Controllers\Base\DashBaseController;

class DashTableController extends DashBaseController 
{
	use TableTrait;

	public function __construct()
	{
		parent::__construct();

		$this->initDependency();
	}

	/*
	|--------------------------------------------------------------------------
	| 
	|--------------------------------------------------------------------------
	*/

	public function getIndex()
	{
		$data['tables'] = $this->tableManager->getAllTables();

		return \View::make('dash.tables.index')->with($data);
	}

	/*
	|--------------------------------------------------------------------------
	| 
	|--------------------------------------------------------------------------
	*/

	public function getCreate()
	{
		return \View::make('dash.tables.create');
	}

	/*
	|--------------------------------------------------------------------------
	| 
	|--------------------------------------------------------------------------
	*/

	public function postCreate()
	{
		return $this->tableManager->storeManager(\Input::all());
	}

	/*
	|--------------------------------------------------------------------------
	| 
	|--------------------------------------------------------------------------
	*/

	/*public function getShow($id)
	{
		$data['table'] = $this->tableManager->gettableById($id);

		return \View::make('dash.tables.show')->with($data);
	}*/

	/*
	|--------------------------------------------------------------------------
	| 
	|--------------------------------------------------------------------------
	*/

	public function getUpdate($id)
	{
		$data['table'] = $this->tableManager->getTableById($id);

		return \View::make('dash.tables.edit')->with($data);
	}

	/*
	|--------------------------------------------------------------------------
	| 
	|--------------------------------------------------------------------------
	*/

	public function putUpdate($id)
	{
		return $this->tableManager->updateManager($id,\Input::all());
	}

	/*
	|--------------------------------------------------------------------------
	| 
	|--------------------------------------------------------------------------
	*/

	public function deleteDelete($id)
	{
		return  $this->tableManager->deleteManager($id);
	}
	
	/*
	|--------------------------------------------------------------------------
	| 
	|--------------------------------------------------------------------------
	*/

	public function getChart()
	{
	
    $tables = $this->tableManager->getAllTables();
	
	$times = \Lava::DataTable();
     
    $times->addStringColumn('Tables Reservation Times')
            ->addNumberColumn('Times');

    // Random Data For Example
	foreach($tables as $row)
	{
    $rowData = array(
      $row->name_en, $row->times_reserved
    );

    $times->addRow($rowData);
	}

	$chart = \Lava::BarChart('myFancyChart');  // Lava::LineChart() if using Laravel

	$chart->datatable($times);
	echo '<div id="myStocks"></div>';
	echo \Lava::render('BarChart', 'myFancyChart', 'myStocks');		
			//return \View::make('dash.tables.chart')->with($data);
	}
	
	/*
	|--------------------------------------------------------------------------
	| 
	|--------------------------------------------------------------------------
	*/
	
	public function resetAll()
	{
		return $this->tableManager->resetAllManager();
	}
}