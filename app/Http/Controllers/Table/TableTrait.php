<?php namespace App\Http\Controllers\Table;

trait tableTrait
{
	public function initDependency()
	{
		$this->tableManager = \App::make('TableManager',['delegate' => $this]);
	}	
}