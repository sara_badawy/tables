<?php namespace App\Http\Managers;

abstract class BaseManager
{
	protected $delegate;
	
	public function __construct($delegate)
	{
		$this->delegate = $delegate;
	}

	/*
	|--------------------------------------------------------------------------
	|
	|--------------------------------------------------------------------------
	*/

	public function __call($name, $arguments)
    {
    	return call_user_func_array( array($this->repo, $name) , $arguments );
	}
}