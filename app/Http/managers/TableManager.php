<?php namespace App\Http\Managers;

class TableManager extends BaseManager
{

	public function __construct($delegate)
	{
		parent::__construct($delegate);

		$this->repo      = \App::make('TableRepo');

		$this->validator = \App::make('TableValidator');
	}

	/*
	|--------------------------------------------------------------------------
	|
	|--------------------------------------------------------------------------
	*/
	
	
	public function storeManager($data)
	{
		$this->validator->store()->validate($data);

		$this->repo->store($data);

		return $this->delegate->storeSuccess('tables.getIndex');
	}	

	/*
	|--------------------------------------------------------------------------
	|
	|--------------------------------------------------------------------------
	*/

	public function updateManager($id,$data)
	{	
		$this->validator->ignore($id)->update()->validate($data);
		
		$this->repo->update($id,$data);

		return $this->delegate->updateSuccess('tables.getIndex');
	}	

	/*
	|--------------------------------------------------------------------------
	|
	|--------------------------------------------------------------------------
	*/

	public function patchManager($id,$data)
	{
		$newData = array_except($data, array('_method'));

		$this->repo->patch($id,$newData);

		return $this->delegate->updateSuccess('tables.getIndex');
	}	

	/*
	|--------------------------------------------------------------------------
	|
	|--------------------------------------------------------------------------
	*/

	public function deleteManager($id)
	{
		$this->repo->delete($id);

		return $this->delegate->deleteSuccess('tables.getIndex');
	}
	
	/*
	|--------------------------------------------------------------------------
	|
	|--------------------------------------------------------------------------
	*/

	public function resetAllManager()
	{
		$this->repo->reset();

		return $this->delegate->updateSuccess('tables.getIndex');
	}
	
}