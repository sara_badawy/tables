<?php namespace App\Http\Repos;

class TableRepo extends BaseRepo
{

    public function __construct()
    {
        parent::__construct();

        $this->model = \App::make('TableModel');
    }
    
	/*
	|--------------------------------------------------------------------------
	|
	|--------------------------------------------------------------------------
	*/

	public function getAllTables()
	{
		return $this->model->get();
	}

	/*
	|--------------------------------------------------------------------------
	|
	|--------------------------------------------------------------------------
	*/

	public function getTableById($id)
	{
		return $this->model->findOrFail($id);
	}

	/*
	|--------------------------------------------------------------------------
	|
	|--------------------------------------------------------------------------
	*/
	
	public function getTableList()
	{
		return $this->model->lists('name_en','id');
	}
	
	/*
	|--------------------------------------------------------------------------
	|
	|--------------------------------------------------------------------------
	*/

	public function store($data)
	{

		$this->model->name_en     = $data['name_en'];

		$this->model->name_ar     = $data['name_ar'];

		$this->model->desc_ar     = $data['desc_ar'];

		$this->model->desc_en     = $data['desc_en'];

		$this->model->save();
	}	

	/*
	|--------------------------------------------------------------------------
	|
	|--------------------------------------------------------------------------
	*/

	public function update($id,$data)
	{
		$this->model = $this->model->findOrFail($id);

		$this->model->name_en     = $data['name_en'];

		$this->model->name_ar     = $data['name_ar'];

		$this->model->desc_ar     = $data['desc_ar'];

		$this->model->desc_en     = $data['desc_en'];
		
		$this->model->save();
	}	

	/*
	|--------------------------------------------------------------------------
	|
	|--------------------------------------------------------------------------
	*/

	public function patch($id,$data)
	{
		$this->model = $this->model->findOrFail($id);
		
		$this->model->{key($data)} = $data[key($data)];

		$this->model->save();
	}

	/*
	|--------------------------------------------------------------------------
	|
	|--------------------------------------------------------------------------
	*/

	public function delete($id)
	{
		$this->model = $this->model->findOrFail($id);

		$this->model->delete();
	}
	
	/*
	|--------------------------------------------------------------------------
	|
	|--------------------------------------------------------------------------
	*/

	public function reset()
	{
		$this->model->update(['is_reserved' => 0]);
	}	
}