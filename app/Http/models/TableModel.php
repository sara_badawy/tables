<?php namespace App\Http\Models;

class TableModel extends BaseModel
{	
	/*
	|--------------------------------------------------------------------------
	| Configs
	|--------------------------------------------------------------------------
	*/
	
	protected $table  = 'tables';

	public $presenter = 'App\Http\Presenters\TablePresenter';

	/*
	|--------------------------------------------------------------------------
	| Relations
	|--------------------------------------------------------------------------
	*/
	
	//

}