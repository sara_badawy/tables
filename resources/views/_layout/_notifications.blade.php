@if (Session::has('success') )

	<div class="alert alert-success alert-dismissable">

		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	
		{{ Session::get('success') }}
	
	</div>

@elseif(Session::has('error'))

	<div class="alert alert-danger alert-dismissable">

		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		@if(is_array(Session::get('error')))

		<ul style="padding-left:20px">

			@foreach(Session::get('error') as $error)

				@foreach($error as $e)

					<li>{{ $e }}</li>

				@endforeach

			@endforeach

		</ul>
		
		@else

			{{ Session::get('error') }}

		@endif

	</div>

@endif