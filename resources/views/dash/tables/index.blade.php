@extends('dash._layout._page')

@section('page-content')

	<div class="page-header">
		<a href="{{ route('dash.tables.getCreate') }}" class="btn btn-primary" type="button">New Table</a>
	</div>

	<div class="page-header">
		<a href="{{ route('dash.tables.getChart') }}" class="btn btn-primary" type="button">Tables Chart</a>
	</div>
	
	<div class="page-header">
		<a href="{{ route('dash.tables.resetAll') }}" class="btn btn-danger" type="button">Reset All Tables</a>
	</div>
	
	@if( !$tables->isEmpty() )

	<table class="table table-striped table-bordered">

		<thead>

			<tr>

				<th>Arabic Name</th>

				<th>English Name</th>

				<th>Is Reserved</th>
				
				<th>Times Reserved</th>
				
				<th>Options</th>

			</tr>

		</thead>

		<tbody>

			@foreach($tables as $row)

			<tr>

				<td><span class="arabic">{{ $row->name_ar }}</span></td>
				
				<td>{{ $row->name_en }}</td>
				
				<td><input type="checkbox" name="my-checkbox" checked>{{ $row->is_reserved }}</td>
				
				<td>{{ $row->times_reserved }}</td>
				
				<td>

					<a data-placement="top" data-toggle="tooltip" type="button" data-original-title="Edit" class="btn btn-xs btn-primary" href="{{ URL::route('dash.tables.getUpdate', $row->id) }}"><span class="glyphicon glyphicon-edit"></span> </a>

					<a href="{{ URL::route('dash.tables.deleteDelete', $row->id) }}" class="btn btn-danger" type="button">Delete</a>

				</td>

			</tr>

			@endforeach

		</tbody>

	</table>

	@else

		<div class="alert alert-info">

			No Records
		
		</div>

	@endif

@stop
<script>
$("[name='my-checkbox']").bootstrapSwitch();
</script>