@extends('dash._layout._page')

@section('page-content')

	<!-- BEGIN FORM-->
	{!! Form::model($table, [ 'method' => 'PUT', 'route' => [ 'dash.tables.putUpdate', $table->id ] , 'class' => 'form-horizontal' ] ) !!}

		<div class="form-group">
			<label class="col-sm-2 control-label">Arabic Name</label>
			<div class="col-sm-3">
				{!! Form::text('name_ar',null,['class'=>'form-control arabic']) !!}
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label">English Name</label>
			<div class="col-sm-3">
				{!! Form::text('name_en',null,['class'=>'form-control']) !!}
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Arabic Describtion</label>
			<div class="col-sm-3">
				{!! Form::text('desc_ar',null,['class'=>'form-control','required']) !!}
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label">English Describtion</label>
			<div class="col-sm-3">
				{!! Form::text('desc_en',null,['class'=>'form-control','required']) !!}
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default">Submit</button>
				<a href="{{ route('dash.tables.getIndex') }}" class="btn btn-default">Cancel</a>
			</div>
		</div>

	{!! Form::close() !!}			
	<!-- END FORM-->

@stop