@extends('dash._layout._page')

@section('page-content')

	
	<div id="stocks-div">
	
	{!! \Lava::render('LineChart', 'myFancyChart', 'myStocks') !!}
	
	</div>

@stop