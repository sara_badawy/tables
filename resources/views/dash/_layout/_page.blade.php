@extends('dash._layout._html')

@section('body')

	<div class="navbar navbar-default navbar-fixed-top" role="navigation">

		<div class="container">

			<div class="navbar-header">

				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			
				<a class="navbar-brand" href="{{ route('dash.tables.getIndex') }}">Tables</a>
			
			</div>{{-- navbar-header --}}

			<div class="navbar-collapse collapse">
	          
				<ul class="nav navbar-nav">

					<li>
						<a href="{{ route('dash.tables.getIndex', 'category=0') }}">
						Tables </a>
					</li>

				</ul>{{-- nav navbar-nav --}}

			</div>{{-- navbar-collapse collapse --}}

		</div>
    
	</div>

	<div class="container">

		@include('_layout._notifications')

		@yield('page-content')

	</div>

@stop