<!DOCTYPE html>

<html lang="en">

<head>

	<title>Dashboard</title>

	<meta charset="utf-8">

    <meta content="IE=edge" http-equiv="X-UA-Compatible">

	<meta content="no-cache" http-equiv="cache-control">
    
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

	<link href="{{ asset('favicon.png') }}" rel="shortcut icon">

	<link href="{{ asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('/assets/plugins/bootstrap-switch/css/bootstrap3/bootstrap-switch.css') }}" rel="stylesheet">
	<link href="{{ asset('/assets/css/table.css') }}" rel="stylesheet">
	<link href="{{ asset('/assets/css/arabic.css') }}" rel="stylesheet">
	<link href="{{ asset('/assets/css/signin.css') }}" rel="stylesheet">
	<link href="{{ asset('/assets/css/page.css') }}" rel="stylesheet">

	<link href="{{ asset('/assets/plugins/select2-3.4.5/select2.css') }}" rel="stylesheet">
	<link href="{{ asset('/assets/plugins/select2-3.4.5/select2-bootstrap.css') }}" rel="stylesheet">

	<link href="{{ asset('/assets/plugins/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css') }}" rel="stylesheet">
	
	<!--<script src="{{ asset('/assets/plugins/jquery-1.10.2.min.js') }}" type="text/javascript"></script>-->
	<script src="{{ asset('/assets/plugins/jquery-2.1.1.min.js') }}" type="text/javascript"></script>
	<script src ="{{ asset('/assets/plugins/bootstrap.min.js') }}"></script>
    <script src ="{{ asset('/assets/plugins/bootstrap-switch/js/bootstrap-switch.js') }}"></script>
	
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	@yield('body')
	
	<script src ="{{ asset('/assets/plugins/select2-3.4.5/select2.js') }}"></script>
	<script src ="{{ asset('/assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>

	<script>

		jQuery(document).ready(function() 
		{
			$(".alert-success").delay(1500).fadeOut("slow", function () { $(this).remove(); });

			$('.btn').tooltip();

			$(".select2").select2();

			$('.switch').bootstrapSwitch();

			$('[data-delete]').click(function(e){
			    e.preventDefault();
			    // If the user confirm the delete
			    if (confirm('Do you really want to delete the element ?')) {
			        // Get the route URL
			        var url = $(this).prop('href');
			        // Get the token
			        var token = $(this).data('delete');
			        // Create a form element
			        var $form = $('<form/>', {action: url, method: 'post'});
			        // Add the DELETE hidden input method
			        var $inputMethod = $('<input/>', {type: 'hidden', name: '_method', value: 'delete'});
			        // Add the token hidden input
			        var $inputToken = $('<input/>', {type: 'hidden', name: '_token', value: token});
			        // Append the inputs to the form, hide the form, append the form to the <body>, SUBMIT !
			        $form.append($inputMethod, $inputToken).hide().appendTo('body').submit();
			    }
			});
		});

	</script>
	
</body>

</html>