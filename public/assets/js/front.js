$(document).ready(function() 
		{
	$('#make').change(function(){
	site_url = $('#site_url').val();
        category_id = $(this).val();
        $('#carType').empty();
        $.ajax({
            type: 'GET',
            url:  site_url+"/api/categories/ajaxCarType",
            data: { category_id: category_id } ,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success:function(veri){
            $.each(veri,function(i,deger){
                  $('#carType').append("<option value='" +deger.id+ "'>" +deger.name_ar+ "</option>" );
                }); //console.log($('#carType'));// each 
            },
            error:function(x,hata){
                //alert("Error : " +hata);
            }
        }); // ajax
     }); // make change
	 
	 $('#moreOptions').click(function(){
		$('#models').removeClass('form-group').addClass('form-group sr-only');
		$('#cities').removeClass('form-group sr-only').addClass('form-group');
		$('#modelsStart').removeClass('form-group sr-only').addClass('form-group');
		$('#modelsEnd').removeClass('form-group sr-only').addClass('form-group');
		$('#moreOptions').removeClass('btn btn-default').addClass('form-group sr-only');
	 }); // moreOptions
	 
	 $('#ModelStart').change(function(){
		model_start = $(this).val();
		//model_start = $(this).find("option:selected").text();
        $('#ModelEnd').empty();
        $.ajax({
            type: 'GET',
            url:  "api/models/ajaxToModel",
            data: { model_start: model_start } ,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success:function(veri){//console.log(veri);
            $.each(veri,function(i,deger){
                  $('#ModelEnd').append('<option value="'+deger.id+'">' +deger.name_ar+ '</option>' );
                }); // each 
            },
            error:function(x,hata){
                //alert("Error : " +hata);
            }
        }); // ajax
	 }); // ModelStart change
	 
	 
	 $("#allModels").change(function() {
		if(this.checked) {
			$('#modelsDiv').find(':checkbox').each(function(){
			$(this).prop('checked', 'checked');
			   });
		} else {
			//$('#modelsDiv').find(':checkbox').each(function(){
			//$(this).prop('checked', false);
			   //});
			 $("#allModels").prop('checked', false);
		}
		}); // allModels
		
		$("#allCities").change(function() {
		if(this.checked) {
			$('#citiesDiv').find(':checkbox').each(function(){
			$(this).prop('checked', true);
			   });
		} else {
			//$('#citiesDiv').find(':checkbox').each(function(){
			//$(this).prop('checked', false);
			   //});
			$("#allCities").prop('checked', false);   
		}
		}); // allCities
		
		$('.chm').click(function() {
			$("#allModels").prop('checked', false);
		});
		
		$('.chc').click(function() {
			$("#allCities").prop('checked', false);
		});
	
$("#comm").keyup(function() {
	total = $("#comm").val();
	total = toArabicNumber(total);
	var factor =0.01;
	var factorDiscount =factor;			
	$("#netComm").html(Math.floor(total * factor));//alert(Math.floor(total * factor));
	$("#commissionvaluediscount").text(Math.floor(total * factorDiscount)); 
}); // comm

$('#followCategory').submit(function(){

var make_str = $("#make :selected").text();//alert(make_str);
$("#makeh").val(make_str);

carType = $("#carType :selected").text();
$("#typeh").val(carType);

model = $("#model :selected").text();
$("#modelh").val(model);

city = $("#city :selected").text();
$("#cityh").val(city);

return true;
}); // followCategory

	
	 });
String.prototype.replaceArray = function(find, replace) {
  var replaceString = this;
  var regex; 
  for (var i = 0; i < find.length; i++) {
    regex = new RegExp(find[i], "g");
    replaceString = replaceString.replace(regex, replace[i]);
  }
  return replaceString;
};
	 
function toArabicNumber (number){
var find = ["۰", "۱", "۲", "۳", "٤", "٥", "٦", "٧", "۸", "9"];
var replace = ["0","1", "2", "3", "4", "5", "6", "7", "8", "9"];
	number = number.replaceArray(find, replace);
	return number;
}	 